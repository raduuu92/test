Taks:
- fork this project;
- implement the design from https://zpl.io/Vx11yK3 (it should be responsive);
- when the `Search` button is clicked it should display the content of the search input.

You can use whatever you like and install additional packages.

Install packages by running `npm install`.

For starting the development server run `npm start`.

Your code should be in the `src/index.js` file and the style in `src/style` folder.

When you finish it send the forked project to `alex@softprovider.ro`.

Good luck!