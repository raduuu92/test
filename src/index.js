import "./style/global.scss";
import React from "react";
import ReactDOM from "react-dom";
import background from './assets/backgroundImage.png';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchString: '',
            displayText: '',
        };
    }
    handleChange = e => {
        this.setState({
            searchString: e.target.value
        })
    }
    handleClear = e => {
        this.setState({
            searchString: '',
            displayText: '',
        })
    }
    handleSubmit = e => {
        e.preventDefault();

        this.setState({
            displayText: this.state.searchString
        })
    }
    render() {
        const { searchString, displayText } = this.state;

        return (
            <div className="root" style={{ backgroundImage: `url(${background})` }}>
                <div className="header">
                    <div className="logoBox">
                        <span className="logo">Logo</span>
                    </div>
                    <button className="loginButton">Login</button>
                </div>
                <div className="content">
                    <div className="title">
                        <span>We make <b>RENTING</b> easy</span>
                    </div>
                    <div className="searchBox">
                        <div className="searchArea">
                            <input 
                                placeholder="CITY, STATE OR ZIP CODE..." 
                                value={searchString}
                                onChange={this.handleChange}
                                className="searchInputField"
                            />
                            <button 
                                className="searchButton"
                                onClick={this.handleSubmit}
                            >
                                SEARCH
                            </button>
                        </div>
                    </div>
                    <div className="displaySearchOutput">
                        {
                            displayText 
                            && <div className="displayOutputBox">
                                <span className="searchOutput">
                                    You have searched for: <b>{displayText}</b>
                                </span>
                                <div className="clearButtonBox">
                                    <button 
                                        className="clearButton"
                                        onClick={this.handleClear}
                                    >
                                        CLEAR
                                    </button>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <Index/>,
    document.getElementById("root")
);